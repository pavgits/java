def buildJar() 
{
    echo 'Building the Application..'
    sh 'mvn package'
}

def buildImage() 
{
    echo "building the Docker image . . "
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo',  passwordVariable: 'PASS',  usernameVariable: 'USER')]) 
    {
             sh 'docker build -t pavn2021/my-repo:1.2 .'
             sh "echo $PASS |docker login -u $USER --password-stdin"
             sh 'docker push pavn2021/my-repo:1.2'
    }

}

def prodApp() 
{
    echo 'Deploying the Application'
    
}
return this

